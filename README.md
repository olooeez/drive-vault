# drive-vault

Drive Vault is a Python application designed to help you create backups of important files on your system and securely sync them with Google Drive. Simplify your backup routine and keep your files safe in the cloud with ease.

## Features

- **Automated Backups**: Select and schedule files or folders to back up regularly.
- **Google Drive Sync**: Seamlessly upload and sync backups with your Google Drive account.
- **File Filtering**: Choose specific file types or exclude certain files from backups.
- **Error Handling**: Robust error detection and logging for seamless operation.

## Prerequisites

Before you begin, ensure you have the following installed:

- Python 3.8+
- [Poetry](https://python-poetry.org/) for dependency management and packaging.

## Installation

Follow these steps to set up the project:

1. Clone the repository:
   ```
   git clone https://gitlab.com/olooeez/drive-vault.git
   cd drive-vault
   ```

2. Install dependencies using Poetry:
   ```
   poetry install
   ```

3. Configure your Google Drive credentials by following the steps in [Google Drive API Documentation](https://developers.google.com/drive/api/v3/quickstart/python).

## Contributing

We welcome contributions from the community! To contribute:

1. Fork the repository.
2. Create a feature branch:
   ```
   git checkout -b feature-name
   ```
3. Make your changes and commit them with a meaningful message.
4. Push to your fork and create a merge request.

## License

This project is licensed under the [MIT License](https://gitlab.com/olooeez/drive-vault/-/blob/main/LICENSE). Refer to the LICENSE file for more details.

## Support

If you encounter any issues or have questions, feel free to open an issue in the [issue tracker](https://gitlab.com/olooeez/drive-vault/-/issues).
